use classicmodels;

select customers.customerNumber,
customerName,
orderNumber,
status
from customers
left join orders on
orders.customerNumber = customers.customerNumber;


select customers.customerNumber,
customerName,
orderNumber,
status
from customers
left join orders on
orders.customerNumber = customers.customerNumber 
where orderNumber is null;

select c.customerNumber,
customerName,
orderNumber,
status
from customers c
left join orders o on
o.customerNumber = c.customerNumber;


-- Using right join show the employee number who is not having any customers.

select employeeNumber,customerNumber from customers right join employees on
salesRepEmployeeNumber = employeeNumber
where customerNumber is null
order by employeeNumber;

-- Use any join to get the productname, productCode, textDescription.
select productName, productCode, textDescription
from products 
join
productlines using (productline);