use classicmodels;
select * from customers;

select count(customerNumber) from customers;
-- select count(employeeNumber) from employees;
select max(creditLimit) from customers;
select customerName, creditLimit from customers order by creditLimit desc limit 1;

select * from employees;
select count(jobTitle) from employees where jobTitle = 'Sales Rep';

select Concat(firstName, ' ' , lastName) as fullName from employees;
select firstName + lastName as FName from employees;

select * from orderdetails;
select *, (quantityOrdered * priceEach) as Amount from orderdetails;
select *, (quantityOrdered * priceEach) as Amount from orderdetails order by Amount desc limit 5;

select * from orders;
select count(customerNumber) 
from orders where shippedDate like '2003%' and status = 'shipped';
select * from orders 
where shippedDate like '2003%' and status = 'shipped';
select count(*) as total_shipped from orders where orderDate 
between '2003-01-01' and '2003-12-31' and status = 'shipped';

-- from --> where --> select --> order by 

select * from employees;

select firstName, lastName from employees 
order by lastName , firstName;
