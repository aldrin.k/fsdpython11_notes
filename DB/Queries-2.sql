-- Give me the order number from orders table who have ordered 
-- between 01/01/2003 and 01/31/2003
use classicmodels;
select orderNumber,orderDate  from orders where orderDate between '2003-01-01' and '2003-01-31';
select orderNumber from orders where orderDate >= '2003-01-01' and orderDate <= '2003-01-31';
select orderNumber from orders 
where extract(year_month from orderDate) = '200301';
select orderNumber, orderDate from orders
where orderDate
between cast('2003-01-01' as Date) and
cast('2003-01-31' as Date);
select orderNumber from orders where Date(orderDate) between '2003-01-01' and '2003-01-31';
select orderNumber from orders where Month(orderDate) = 1 and year(orderDate) = 2003;

-- Give productcode, productname, buyprice from products table
-- who's buy price is 90 - 100
select productCode, productName, buyPrice from products 
where buyPrice between 90 and 100;
select productCode, productName, buyPrice from products 
where buyPrice >= 90 and buyPrice <= 100;
-- select productCode, productName, buyPrice from products 
-- where buyPrice like '9%';

-- Give customername, country, state, creditlimit from customers
-- who belongs to USA and CA having a creditlimit more than 100000;

select customerName, country, state, creditLimit from customers
where country = 'USA' and state = 'CA' and creditLimit > 100000;
select customerName, country, state, creditLimit from customers
where country in ('USA') and state in ('CA') and creditLimit > 100000;

-- how many states are there in cutomers tables?
select count(state) from customers;
select distinct(state) from customers;
select count(distinct state) as states from customers where state is not null;

