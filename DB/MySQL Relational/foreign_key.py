'''
Foreign Key - This is a relation between two tables on same database. Following are the conditions to make a foreign key

1. To create a foreign key we require the tables in same database.
2. The fields which we used in the foreign key relationship must be indexed (numbered).
3. Both the columns should have same datatype.

Let's create a foreign key,
but first we require two tables in a database, we already have one with student_id as a primary key
'''

import mysql.connector

# connecting to the local DB server

conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='aldrin',
    database='fsd11',

)

# inserting data on to first table

# Query = '''INSERT INTO student_details VALUES 
# (6, 'Mohit', 'Ranchi'),
# (7, 'Ad', 'Banglore'),
# (8, 'Ashish k', 'Hyderabad'),
# (9, 'Sudeep', 'Banglore'),
# (10, 'Barat','Bhubaneswar');'''

# Checking the entries (I have added more data)

# Query = '''
# Select * from student_details;
# '''
# db = conn.cursor()
# db.execute(Query)
# for i in db:
#     print(i)

'''
Output -> s
(1, 'rohit', 'Ranchi')
(2, 'Ed', 'Banglore')
(3, 'Ashish', 'Hyerabad')
(4, 'Sudeept', 'Bangalore')
(5, 'Bharat', 'Bhubaneswar')
(6, 'Mohit', 'Ranchi')
(7, 'Ad', 'Banglore')
(8, 'Ashish k', 'Hyderabad')
(9, 'Sudeep', 'Banglore')
(10, 'Barat', 'Bhubaneswar')
'''

# Lets create another table but with a foreign key, 

# Query = '''
# Create table Marks(
# Student_id int,
# Maths int,
# Physics int,
# Python int,
# English int,
# Foreign key(Student_id) References student_details(student_id))
# '''

# Now that we have created the tables and it has foreign key as Student_id
# Lets add datas to table 2

Query = """
Insert into marks values 
(1, 78, 77, 89, 99),
(2, 68, 88, 89, 88),
(3, 58, 77, 100, 55),
(4, 78, 67, 89, 95),
(5, 78, 88, 89, 45)
"""
db = conn.cursor()
db.execute(Query)
conn.commit()
conn.close()

# Inserted few datas 
# To fetch the datas we need to join the tables and we can fetch the data