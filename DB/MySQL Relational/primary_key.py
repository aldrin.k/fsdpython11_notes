'''MySQL consists of 5 keys:
Primary Key
Foreign Key
Candidate Key
Unique Key
Super Key

Primary Key - This is an unique constraint, which doesn't allow duplicate, null values.
A table can only have one primary key.

Let's create a primary key
'''

import mysql.connector

# connecting to the local DB server

conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='aldrin',
    database='fsd11'
)

Query = '''CREATE TABLE student_details(
student_id INT AUTO_INCREMENT,
student_name VARCHAR (50),
student_address VARCHAR (100),
PRIMARY KEY (student_id))'''

db = conn.cursor()
db.execute(Query)
conn.commit()
conn.close()

# Use 'describe student_details' to see if 
# we have set student_id as primary key, also you'll notice we have an auto increment
