import mysql.connector

def connectingDB():
    connection = mysql.connector.connect(
        host='localhost',
        user='root',
        password='aldrin')                      #-> Connecting to MySQL
    cursor = connection.cursor()                #-> Setting the Cursor
    Query = 'create database fsd11'             #-> 'Query' is like an object, Create database will
                                                #   create a database when Executed with the name fsd11
    cursor.execute(Query)                       #-> execute will execute the command in Query
    print('Executed !!')
    
connectingDB()