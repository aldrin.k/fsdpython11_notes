import mysql.connector

# CRUD - > Create, Retrive, Update, Delete

def retrive():
    conn = mysql.connector.connect(
        host="localhost",
        user='root',
        password='aldrin',
        database='fsd11'
    )
    db = conn.cursor()
    # db.execute("Select * from student_details")

    # print(type(db))
    # for i in db:
    #     print(i)

    # using python
    # for i in db:
    #     if i[1][0] == 'A':
    #         print(i)
    
    # using SQL Query
    # db.execute("Select * from student_details where name like 'A%' ")
    # db.execute("Select * from student_details where passout_year = '2022-4-30'")
    # db.execute("Select * from student_details where passout_year >= '2022-04-01' and passout_year <= '2022-04-30' ")
    # db.execute("Select * from student_details where passout_year like '______4%' and passout_year like '2022%' ")

    db.execute("Select * from student_details where slno %3 = 0")
    for i in db:
        print(i)
    conn.close()
retrive()

