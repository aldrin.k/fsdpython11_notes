"""
Date: 24-03-2023
Author: 
Project: Resturant management system
"""

# Create a Program which helps the Resturant Manager to:
# Create a resturant
# add number of tables to it
# insert food items along with the prices.

# Create another program which works as an client/customer and should have the following operations, 
# Book table
# Order food
# Cancel booked table
# Generate bill
# Save the each bill as a text file using file handling.
# Make this program in an executable file in terminal ie. .py file and submit your file in LMS.


class Restaurant:
    def __init__(self):
        Flag = True
        print('Please Enter the resturant name: ')
        self.ResturantName = input()
        print('Please Enter the number of tables you want in',self.ResturantName)
        NumberTable = int(input())
        self.Table_details = {i: None for i in range(1,NumberTable+1)}
        print(f'Enter the number of food items you want to keep:')
        self.food_items = dict()
        count = int(input())
        while count > 0:
            count -= 1
            foodi = input('Enter the food item: ')
            self.food_items[foodi] = input(f'Enter the price of {foodi}: ')
        print(f'{self.ResturantName} was successfully opened !!')
        print(self.food_items, self.Table_details)
    
    def Book_table(self, table_number):
        if table_number in self.Table_details.keys():
            self.Table_details[table_number] = 'Booked'
            print('Table booked successfully')
        else:
            if table_number in range(1,16) and not self.Table_details[table_number] == 'Booked':
                print('Table Not Vacant, please choose another table')
            else:
                print('Enter a valid table number')


    
object = Restaurant()
object.Book_table(5)
object.Book_table(5)
object.Table_details