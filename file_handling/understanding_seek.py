file = open('y.txt','rb')
import os
# os.SEEK_CUR - 1, os.SEEK_SET - 0, os.SEEK_END - 2

# print(file.seekable())
# file.seek(5)
# print(file.read())
# print('----------')

print(f'The current position of the cursor', file.tell())
file.seek(5)
print(f'The current position of the cursor', file.tell())
print(file.seek(10,os.SEEK_CUR))
print(file.read())
file.close()